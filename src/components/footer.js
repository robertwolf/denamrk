import React from "react"

import styled from "@emotion/styled"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEnvelope, faPhone } from "@fortawesome/free-solid-svg-icons"
import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons"

import Section from "./section"

import { rhythm } from "../utils/typography"

const Links = styled.div`
  display: flex;
  padding: ${rhythm(2)} 0;

  p {
    margin: 0;
  }

  a {
    color: black;
    text-decoration: none;
    display: block;
    padding: 0 ${rhythm(1)};
  }

  svg {
    opacity: 0.5;
    margin-bottom: ${rhythm(.5)};
  }

  @media screen and (max-width: 600px) {

    flex-direction: column;

    a {
      padding: ${rhythm(.25)} 0;
      text-decoration: underline;
    }

    svg {
      display: none;
    }
  }
`

export default () => (
  <Section layout={["center"]} title="Contact me">
    <div>
      <h2>Are you interested?</h2>
      <p>Get to know me better 😊</p>
      <Links>
        <a href="mailto:robert@robertwolf.dk">
          <FontAwesomeIcon icon={faEnvelope} />
          <p>robert@robertwolf.dk</p>
        </a>
        <a href="tel:+4591999049">
          <FontAwesomeIcon icon={faPhone} />
          <p>+45 91 99 90 49</p>
        </a>
        <a href="https://www.linkedin.com/in/wolfr" target="_blank" rel="noopener noreferrer">
          <FontAwesomeIcon icon={faLinkedinIn} />
          <p>linkedin.com/in/wolfr</p>
        </a>
      </Links>
    </div>
  </Section>
)
