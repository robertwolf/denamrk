import React, { useState, useEffect } from "react"
import sal from "sal.js"
import styled from "@emotion/styled"

const addAnimation = (selector, type, options) => {
  const elements = document.querySelectorAll(selector)
  if (elements === null) return

  elements.forEach(el => {
    el.setAttribute("data-sal", type)

    if (options === undefined) return

    for (var key in options) {
      if (options.hasOwnProperty(key)) {
        el.setAttribute(`data-sal-${key}`, options[key])
      }
    }
  })
}

const Layout = styled.article`
  opacity: ${p => (p.loaded ? 1 : 0)};

  .char {
    transition-delay: calc(
      var(--char-index) / var(--word-index) * 140ms
    ) !important;
  }

  [data-sal="underline"] {
    position: relative;
    overflow: hidden;
    display: inline-block;

    &::after {
      content: "";
      position: absolute;
      height: 3px;
      width: 100%;
      background: black;
      left: 0;
      bottom: 1px;
      opacity: calc(0.5 - var(--word-index) / 20);
      transform: translateX(-105%);
      transition: transform 1.7s ease-in-out;
      transition-delay: calc(
        var(--word-index) * 0.1s / (var(--line-index) + 1)
      ) !important;
    }

    &.sal-animate {
      &::after {
        transform: translateX(105%);
      }
    }
  }
`

export default ({ children }) => {
  const [loaded, setLoaded] = useState(false)

  // DOM init
  useEffect(() => {
    if (typeof document === `undefined`) return
    if (typeof window === `undefined`) return

    const Splitting = require("splitting")
    Splitting()

    Splitting({
      target: "h2",
      by: "lines",
    })

    addAnimation(".char", "fade", {
      duration: "1000",
      delay: "1000",
      easing: "ease-in-out",
    })

    addAnimation("h2 .word", "underline", {
      duration: "1000",
      delay: "1000",
      easing: "ease-in-out",
    })

    sal({
      threshold: 0.3,
    })

    setLoaded(true)
  }, [])

  return <Layout loaded={loaded}>{children}</Layout>
}
