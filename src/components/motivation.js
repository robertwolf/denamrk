import React from "react"

import { css } from "@emotion/core"
import { rhythm } from "../utils/typography"

import Section from "../components/section"

export default ({children}) => (
    <Section layout={["center"]} title="My motivation">
    <div>
      <blockquote
      data-splitting='chars'
        css={css`
          max-width: 20em;
          font-size: ${rhythm(1)};
          font-style: italic;
          line-height: ${rhythm(2)};
          margin-top: 3vw;
          margin-bottom: 10vw;
        `}
      >
        {children}
      </blockquote>
    </div>
  </Section>
)