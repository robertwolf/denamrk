import React, { useState, useEffect } from "react"

import styled from '@emotion/styled'

import {rhythm} from '../utils/typography'

const Section = styled.section`
  width: 87.5vw;
  padding: ${rhythm(1)} 0;
  margin: 0 auto;
  border-top-style: ${p => p.borderStyle};
  border-color: #E5E5E5;
  border-width: ${p => p.borderStyle === 'double' ? '3px' : '1px'};
  display: flex;
  flex-wrap: wrap;


  min-height: ${() => typeof window !== `undefined` ? window.innerHeight + 'px' : '100vh'};
  align-items: center;
  overflow: hidden;

  > h5 {
    padding-top: 0;
    padding-bottom: 4vw;
    margin-top: 0;
    width: 100%;
    text-align: left;
  }

  /* Layouts */

  &.center {
    justify-content: center;
    text-align: center;
    align-items: unset;

    > div {
      margin: auto;
    }
  }


  &.columns {
    flex-direction: row;
    
    > div {
      flex-grow: 1;

      @media screen and (min-width: 768px) {
        width: 50%;
        padding: 6.25vw;
      }
    }
  }

  &.reverse {
    flex-direction: row-reverse;
  }


`

export default ({title, children, borderStyle, layout}) => {

  // useEffect(() => {
  //   if (typeof window === `undefined`) return;

  //   window.addEventListener('resize', () => {
  //     let vh = window.innerHeight * 0.01;
  //     document.documentElement.style.setProperty('--vh', `${vh}px`);
  //   });

  // },[])

  if (borderStyle === undefined) borderStyle = 'solid';
  if (layout === undefined) layout = [];
  
  return (
    <Section borderStyle={borderStyle} className={layout.join(' ')}>
      {title && <h5>{title}</h5>}
      {children}
    </Section>
  )}