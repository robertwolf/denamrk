import React from "react"
import AnchorLink from 'react-anchor-link-smooth-scroll'

import {css} from '@emotion/core'

export default () => (
  <AnchorLink href="#firstReason" css={css`margin-top: auto;
  width: 100%;
  text-decoration: none;`}>
      <h5  data-sal="slide-up" data-sal-duration="500" data-sal-delay="5000">See Why ↓</h5>
      </AnchorLink>
  )