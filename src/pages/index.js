import React from "react"

import SEO from "../components/seo"

import Section from "../components/section"

const IndexPage = () => (
  <>
    <SEO title="Robert Wolf" />
    <Section layout={['center', 'fullscreen']}>
      <div>
          <h1>Hi, I am <b>Robert Wolf</b></h1>
          <p>Digital space is my ocean</p>
          <p><a href="mailto:work@robertwolf.cz">
          <p>work@robertwolf.cz</p>
        </a></p>
      </div>

    </Section>
  </>
)

export default IndexPage
