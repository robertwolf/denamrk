import React, { useEffect } from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import '../../node_modules/sal.js/dist/sal.css';

import { css } from "@emotion/core"

import { rhythm } from "../utils/typography"

import logo from "../images/kanpla.png"


import Layout from "../components/layout"
import Motivation from "../components/motivation"
import Footer from "../components/footer"
import SEO from "../components/seo"
import SeeWhy from "../components/seeWhy"
import Section from "../components/section"



export default () => {

  // Get images
  const data = useStaticQuery(getImages)

  return (
    <Layout>
      <SEO title="Robert Wolf for Kanpla" />
      <Section borderStyle="none" layout={["center", "fullscreen"]}>
        <div>
          <img data-sal="slide-up" data-sal-duration="1500" data-sal-delay="1500" src={logo} css={{ width: "30px", marginTop: "10vh" }} />
          <h1 data-sal="slide-up" data-sal-duration="1500" data-sal-delay="0">
            Hi <b>Kanpla</b>
          </h1>
          <p data-sal="slide-up" data-sal-duration="1000" data-sal-delay="300">
            <span>My name is Robert Wolf, and I am</span>{" "}
            <span>the programmer you are looking for.</span>
          </p>
        </div>
        <SeeWhy />
      </Section>
      <div id="firstReason">
        <Section title="Reason n.1" layout={["columns"]}>
          <div>
            <h2>I have 3+ years of experience in web development.</h2>
            <p>
              I am skilled in technologies like JavaScript and its frameworks, and <b>I can create all the functionalities your web-app will need.</b>
          
            </p>
            <p>
              For example, I have developed an online booking system for a
              fishing destination in Norway as a freelance project<span css={css`@media screen and (min-width: 768px) { display: none;}`}>.</span><span css={css`@media screen and (max-width: 768px) { display: none;}`}> →</span>
            </p>
          </div>
          <div
            css={css`
              position: relative;
              /* overflow: hidden; */
              min-height: 200px;
              align-self: stretch;
            `}

          data-sal="slide-up"
          data-sal-duration="500"
          data-sal-delay="100"
          >
            <Img
              css={css`
                position: absolute !important;
                top: 0;
                left: 12.5%;
                width: 75%;
              `}
              fluid={data.placeholderImage.childImageSharp.fluid}
            />
          </div>
        </Section>
      </div>
      <Section title="Reason n.2" layout={["columns", "reverse"]}>
        <div>
          <h2>I thrive in the startup culture.</h2>
          <p>
            I putted together a team which won a 3rd place in the San Francisco
            JAMStack Hackathon. We combined the most modern web technologies
            with AI to create an innovative language learning web-app.
          </p>
          <p css={css`@media screen and (max-width: 768px) { display: none;}`}>← Watch me presenting the idea (I am the one with the black T-shirt)</p>
        </div>
        <div >
          <iframe
            css={css`
              width: 100% !important;
              height: 275px !important;
            `}
            data-sal="fade"
            data-sal-duration="1000"
            data-sal-delay="100"
            src="https://www.youtube.com/embed/QhybHEs87mk?start=920"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>
      </Section>
      <Section title="Reason n.3" layout={["columns"]}>
        <div>
          <h2>I connect programming with business thinking.</h2>
          <p>
            I have completed an AP degree in Multimedia Design
            and Communication and I am currently pursuing a bachelor’s degree in
            Innovation and Entrepreneurship. <b>I can understand and use your business goals in developing the app</b>.
          </p>
          <p>
            {" "}
            <a href="/robertwolf-kanpla.pdf" target="_blank">
              Download my resume ↓
            </a>
          </p>
        </div>
        <div 
          data-sal="fade"
          data-sal-duration="1000"
          data-sal-delay="100">
          <Img
            css={css`
              margin-bottom: ${rhythm(1)};
            `}
            fluid={data.businessImage.childImageSharp.fluid}
          />
        </div>
      </Section>
      <Motivation>I'd like to help Kanpla to develop a <b>great digital solution that would secure the company success</b>, because I strongly feel the importance of reducing foodwaste in the modern world.</Motivation>
      <Footer />
    </Layout>
  )
}

const getImages = graphql`
  query {
    placeholderImage: file(name: { eq: "fiskeferie" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    businessImage: file(name: { eq: "business-thinking" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
