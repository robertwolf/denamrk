import React, { useEffect } from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import '../../node_modules/sal.js/dist/sal.css';

import { css } from "@emotion/core"

import { rhythm } from "../utils/typography"

import logo from "../images/lingoowl.png"


import Layout from "../components/layout"
import Motivation from "../components/motivation"
import Footer from "../components/footer"
import SEO from "../components/seo"
import SeeWhy from "../components/seeWhy"
import Section from "../components/section"



export default () => {

  // Get images
  const data = useStaticQuery(getImages)

  return (
    <Layout>
      <SEO title="Robert Wolf for Lingoowl" />
      <Section borderStyle="none" layout={["center", "fullscreen"]}>
        <div>
          <img data-sal="slide-up" data-sal-duration="1500" data-sal-delay="1500" src={logo} css={{ width: "60px", marginTop: "10vh" }} />
          <h1 data-sal="slide-up" data-sal-duration="1500" data-sal-delay="0">
            Hi <b>Lingoowl</b>
          </h1>
          <p data-sal="slide-up" data-sal-duration="1000" data-sal-delay="300">
            <span>My name is Robert Wolf, and I am</span>{" "}
            <span>the react talent you are looking for.</span>
          </p>
        </div>
        <SeeWhy />
      </Section>
      <div id="firstReason">
        <Section title="Reason n.1" layout={["columns"]}>
          <div>
            <h2>React is my daily bread.</h2>
            <p>
              I have 3+ years of experience in web development. <b>I can code pixel perfect UI and capture complex logic in a readable code.</b>
          
            </p>
            <p>
              One of my latest project is a complex kanteen lunch booking system for Kanpla.<span css={css`@media screen and (min-width: 768px) { display: none;}`}>.</span><span css={css`@media screen and (max-width: 768px) { display: none;}`}> →</span>
            </p>
          </div>
          <div
            css={css`
              position: relative;
              /* overflow: hidden; */
              min-height: 200px;
              align-self: stretch;
            `}

          data-sal="slide-up"
          data-sal-duration="500"
          data-sal-delay="100"
          >
            <Img
              css={css`
                position: absolute !important;
                top: 0;
                left: 0;
                width: 100%;
              `}
              fluid={data.placeholderImage.childImageSharp.fluid}
            />
          </div>
        </Section>
      </div>
      <Section title="Reason n.2" layout={["columns", "reverse"]}>
        <div>
          <h2>I thrive in the startup culture.</h2>
          <p>
            I putted together a team which won a 3rd place in the San Francisco
            JAMStack Hackathon. We used React and AI to create an innovative language learning web-app.
          </p>
          <p css={css`@media screen and (max-width: 768px) { display: none;}`}>← Watch a bit of my presentation</p>
        </div>
        <div >
          <iframe
            css={css`
              width: 100% !important;
              height: 275px !important;
            `}
            data-sal="fade"
            data-sal-duration="1000"
            data-sal-delay="100"
            src="https://www.youtube.com/embed/QhybHEs87mk?start=920"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>
      </Section>
      <Section title="Reason n.3" layout={["columns"]}>
        <div>
          <h2>I care about the user.</h2>
          <p>
            With background in Multimedia Design and Communication, I complement my programming skills with a solid design and business understanding. <b>I can provide Lingoowl with a great User Experience</b>.
          </p>
          <p>
            {" "}
            <a href="/robertwolf-lingoowl.pdf" target="_blank">
              Download my resume ↓
            </a>
          </p>
        </div>
        <div 
          data-sal="fade"
          data-sal-duration="1000"
          data-sal-delay="100">
          <Img
            css={css`
              margin-bottom: ${rhythm(1)};
            `}
            fluid={data.businessImage.childImageSharp.fluid}
          />
        </div>
      </Section>
      <Motivation>I myself speak 5 diffrent languages, so <b>working on a excellent translation service is very meaningful to me.</b> I would love to become a part of the danish work culture and grow along with Lingoowl.</Motivation>
      <Footer />
    </Layout>
  )
}

const getImages = graphql`
  query {
    placeholderImage: file(name: { eq: "kanpla-screen" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    businessImage: file(name: { eq: "business-thinking" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
