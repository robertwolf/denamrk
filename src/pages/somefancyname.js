import React, { useEffect } from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import '../../node_modules/sal.js/dist/sal.css';

import { css } from "@emotion/core"

import { rhythm } from "../utils/typography"


import Layout from "../components/layout"
import Motivation from "../components/motivation"
import Footer from "../components/footer"
import SEO from "../components/seo"
import SeeWhy from "../components/seeWhy"
import Section from "../components/section"



export default () => {

  // Get images
  const data = useStaticQuery(getImages)

  return (
    <Layout>
      <SEO title="Robert Wolf for Kanpla" />
      <Section borderStyle="none" layout={["center", "fullscreen"]}>
        <div>
          {/* <img data-sal="slide-up" data-sal-duration="1500" data-sal-delay="1500" src={logo} css={{ width: "30px", marginTop: "10vh" }} /> */}
          <h1 data-sal="slide-up" data-sal-duration="1500" data-sal-delay="0" css={css`margin-bottom: 0; margin-top: 15vh;`}>
            <b>Hello</b>
          </h1>
          <p  data-sal="slide-up" data-sal-duration="1500" data-sal-delay="200" css={css`letter-spacing: .1em; text-transform: uppercase; font-size: ${rhythm(0.5)}; margin-bottom: ${rhythm(1)};`}>somefancyname</p>
          <p data-sal="slide-up" data-sal-duration="1000" data-sal-delay="1000">
            My name is Robert Wolf, and I am <br/>the front-end dev you are looking for.
          </p>
        </div>
        <SeeWhy />
      </Section>
      <div id="firstReason">
        <Section title="Reason n.1" layout={["columns"]}>
          <div>
            <h2>React and GraphQL is my daily bread.</h2>
            <p>
              I have 3+ years experience in web-development. I work independently and with an attention to detail, <b>so I will ensure beautiful and flawless experience for your users.</b>
          
            </p>
            <p>
              For example, during my internship at <a href="https://en.studiogusto.com/" target="_blank" rel="noopener noreferrer">Gusto IDS</a> I have independently designed and developed a website for a famous Italian architect Marco Acerbis.
            </p>
          </div>
          <div

          data-sal="slide-up"
          data-sal-duration="500"
          data-sal-delay="100"
          >
            <Img
              css={css`
                max-height: 65vh;
                max-width: 40vh;
                margin: 0 auto;
              `}
              fluid={data.acerbisImage.childImageSharp.fluid}
            />
          </div>
        </Section>
      </div>
      <Section title="Reason n.3" layout={["columns", "reverse"]}>
        <div>
          <h2>I connect programming with business thinking.</h2>
          <p>
            I have completed an AP degree in Multimedia Design
            and Communication and I am currently pursuing a bachelor’s degree in
            Innovation and Entrepreneurship. <b>I work consistently from business research and user experience design to the last line of code.</b>
          </p>
          <p>
            {" "}
            <a href="/robertwolf-somefancyname.pdf" target="_blank">
              Download my resume ↓
            </a>
          </p>
        </div>
        <div 
          data-sal="fade"
          data-sal-duration="1000"
          data-sal-delay="100">
          <Img
            css={css`
              margin-bottom: ${rhythm(1)};
            `}
            fluid={data.businessImage.childImageSharp.fluid}
          />
        </div>
      </Section>
      <Section title="Reason n.2" layout={["columns"]}>
        <div>
          <h2>I thrive in the startup culture.</h2>
          <p>
            I putted together a team which won a 3rd place in the San Francisco
            JAMStack Hackathon. We combined the most modern web technologies
            with AI to create an innovative language learning web-app.
            </p>
            <p><b>I am a 100% team-player.</b> Let's build great things together!</p>
        </div>
        <div >
          <iframe
            css={css`
              width: 100% !important;
              height: 275px !important;
            `}
            data-sal="fade"
            data-sal-duration="1000"
            data-sal-delay="100"
            src="https://www.youtube.com/embed/QhybHEs87mk?start=920"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>
      </Section>
      <Motivation>Sustainable fashion is very important to me. I'd like to help you <b>grow and appeal to more people</b> so together have a positive impact on our planet.</Motivation>
      <Footer />
    </Layout>
  )
}

const getImages = graphql`
  query {
    acerbisImage: file(name: { eq: "acerbis" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    businessImage: file(name: { eq: "business-thinking" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
