import Typography from "typography"

const typography = new Typography({
  baseFontSize: "18px",
  baseLineHeight: 1.666,
  headerFontFamily: [
    "Avenir Next",
    "Helvetica Neue",
    "Segoe UI",
    "Helvetica",
    "Arial",
    "sans-serif",
  ],
  bodyFontFamily: [
    "Avenir Next",
    "Helvetica Neue",
    "Segoe UI",
    "Helvetica",
    "Arial",
    "sans-serif",
  ],
  overrideStyles: () => ({
    'h1': {
        fontWeight: 400,
        fontSize: rhythm(2),
    },
    'h2': {
        lineHeight: rhythm(1.25),
        marginBottom: rhythm(.25),
    },
    'h5': {
        textTransform: 'uppercase',
        fontWeight: 400,
        letterSpacing: '.1em',
        color: '#808080',
        fontSize: rhythm(.45),
    },
    'a': {
        color: 'black'
    },
  }),
})

export default typography

export const rhythm = typography.rhythm